package org.example.entity.enums;

public enum Job {
    CASHIER, MANAGER, OTHER, BRANCH_MANAGER, MAINTENANCE_STAFF
}
