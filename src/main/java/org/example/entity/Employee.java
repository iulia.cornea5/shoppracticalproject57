package org.example.entity;

import jakarta.persistence.*;
import org.example.entity.enums.Job;

// many employees belongs to one branch
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING)
    private Job job;

    @ManyToOne
    private Branch branch;

    public Employee() {
    }

    public Employee(Integer id, String name, Job job, Branch branch) {
        this.id = id;
        this.name = name;
        this.job = job;
        this.branch = branch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", job=" + job +
                ", branch=" + branch +
                '}';
    }
}
