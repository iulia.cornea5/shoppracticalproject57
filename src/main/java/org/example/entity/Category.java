package org.example.entity;

import jakarta.persistence.*;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @Column(name = "available_under_18")
    private Boolean availableUnder18;

    public Category() {
    }

    public Category(Integer id, String name, Boolean availableUnder18) {
        this.id = id;
        this.name = name;
        this.availableUnder18 = availableUnder18;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAvailableUnder18() {
        return availableUnder18;
    }

    public void setAvailableUnder18(Boolean availableUnder18) {
        this.availableUnder18 = availableUnder18;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", availableUnder18=" + availableUnder18 +
                '}';
    }
}
