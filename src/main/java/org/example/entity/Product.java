package org.example.entity;

import jakarta.persistence.*;

import java.util.List;


// by default, când citesc un produs din baza de date vreau sa NU fie incarcate si categoriile (LAZY fetch)
// așa ca pentru cazul in care vreau sa fie incarcate si categoriile fiecarui produs o sa fac o metoda separata

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    private String name;

    private String brand;

    private Double price;

    @Column(name = "value_price")
    private Double valuePrice;

    private Integer quantity;

    @ManyToOne
    private Supplier supplier;

    // @ManyToMany(fetch = FetchType.EAGER) <- eager = aduce categoriile din baza de date de fiecare data când aduce un produs
    @ManyToMany
    List<Category> categories;

    public Product() {
    }

    public Product(Integer id, String name, String brand, Double price, Double valuePrice, Integer quantity, Supplier supplier) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.valuePrice = valuePrice;
        this.quantity = quantity;
        this.supplier = supplier;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getValuePrice() {
        return valuePrice;
    }

    public void setValuePrice(Double valuePrice) {
        this.valuePrice = valuePrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", categories='" + categories + '\'' +
                ", price=" + price +
                ", valuePrice=" + valuePrice +
                ", quantity=" + quantity +
                ", supplier=" + supplier +
                "}\n";
    }
}
