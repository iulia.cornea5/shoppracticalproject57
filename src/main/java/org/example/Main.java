package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.Branch;
import org.example.entity.Supplier;
import org.example.repository.BranchRepository;
import org.example.repository.SupplierRepository;
import org.hibernate.SessionFactory;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        System.out.println("Trying to open session");
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        System.out.println("Session open");

        BranchRepository branchRepository = new BranchRepository(sessionFactory);
        Branch b1 = new Branch(null, "METRO-Cluj", "Cluj");
        Branch b2 = new Branch(null, "Maga Image", "Timișoara");
        Branch b3 = new Branch(null, "METRO-București", "București");
        Branch b4 = new Branch(null, "IKEA", "Timișoara");
        branchRepository.create(b1);
        branchRepository.create(b2);
        branchRepository.create(b3);
        branchRepository.create(b4);

        Branch readBranch = branchRepository.readById(2);
        System.out.println(readBranch);

        List<Branch> readAllBranchList = branchRepository.readAll();
        System.out.println(readAllBranchList);

        b1.setName("New rebranded METRO-Clu pastej");
        branchRepository.updateDetails(b1);

        branchRepository.delete(b2);

        readAllBranchList = branchRepository.readAll();
        System.out.println(readAllBranchList);


        Supplier s1 = new Supplier(null, "Ceva 1 SRL", "Cluj");
        Supplier s2 = new Supplier(null, "Ceva 2 SRL", "Cluj");
        Supplier s3 = new Supplier(null, "Ceva 3 SRL", "Cluj");
        Supplier s4 = new Supplier(null, "Ceva 4 SRL", "Cluj");


        SupplierRepository sr = new SupplierRepository(sessionFactory);
        sr.create(s1);
        sr.create(s2);
        sr.create(s3);
        sr.create(s4);

        s2.setName("Ceva 22 SRL");
        sr.updateDetails(s2);

        sr.delete(s3);

        System.out.println(sr.readAll());

    }
}