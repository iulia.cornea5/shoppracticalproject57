package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.Branch;
import org.example.entity.Employee;
import org.example.entity.enums.Job;
import org.example.repository.BranchRepository;
import org.example.repository.EmployeeRepository;

public class MainQueries {

    private static BranchRepository branchRepository = new BranchRepository(HibernateConfiguration.getSessionFactory());
    private static EmployeeRepository employeeRepository = new EmployeeRepository(HibernateConfiguration.getSessionFactory());

    public static void main(String[] args) {
        populateDB();
        // Afișați toate Branch-urile care se află în "Brașov"
        System.out.println("Branches in Brașov: " + branchRepository.findByLocation("Brașov"));


        // Afișați toate Branch-urile care conțin în denumire cuvântul "Auchan"
        // sql: SELECT * FROM branch WHERE name LIKE '%Auchan%'

        System.out.println("Branches 'ro-B': " + branchRepository.findByWordInName("ro-B"));

        // Afișați toți angajații cu funcție de manager (indiferent daca e branch manager sau executive manager)
        // folositi parameter in query

        System.out.println("Manager employees: " + employeeRepository.findByJob(Job.MANAGER));
    }

    public static void populateDB() {
        Branch b1 = new Branch(null, "Metro-Cluj", "Cluj-Napoca");
        Branch b2 = new Branch(null, "Metro-Brașov", "Brașov");
        Branch b3 = new Branch(null, "Metro-București", "București");
        branchRepository.create(b1);
        branchRepository.create(b2);
        branchRepository.create(b3);


        Employee employee1 = new Employee(null, "Buda Cristian", Job.CASHIER, b1);
        Employee employee2 = new Employee(null, "Marius Dumitrache", Job.MANAGER, b2);
        Employee employee3 = new Employee(null, "Marian Frizeru", Job.BRANCH_MANAGER, b2);
        Employee employee4 = new Employee(null, "Ombladon Cheloo", Job.CASHIER, b2);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);

    }
}
