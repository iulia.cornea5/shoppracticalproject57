package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.Category;
import org.example.entity.Product;
import org.example.entity.Supplier;
import org.example.repository.CategoryRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;

import java.util.List;

public class MainLazyLoading {

    private static SupplierRepository supplierRepository = new SupplierRepository(HibernateConfiguration.getSessionFactory());
    private static ProductRepository productRepository = new ProductRepository(HibernateConfiguration.getSessionFactory());
    private static CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());


    public static void main(String[] args) {
        populateDb();

        productRepository.readAllWithPrintInsideSession();
        System.out.println("In main:");
        System.out.println(productRepository.readAllWithCategories());

    }



    private static void populateDb() {
        Supplier s1 = new Supplier(null, "Dorna", "Vatra Dornei");
        Supplier s2 = new Supplier(null, "Acvila", "Luduș");
        supplierRepository.create(s1);
        supplierRepository.create(s2);

        Category alcohol = new Category(null, "alcohol", Boolean.FALSE);
        Category beverages = new Category(null, "beverages", Boolean.TRUE);
        categoryRepository.create(alcohol);
        categoryRepository.create(beverages);

        Product p1 = new Product(null, "Apa minerala", "Dorna", 2.0, 3.0, 500, s1);
        p1.setCategories(List.of(beverages));
        Product p2 = new Product(null, "Apa plata", "Dorna", 2.5, 3.0, 1000, s1);
        p2.setCategories(List.of(beverages));
        Product p3 = new Product(null, "Bere", "Tuborg", 2.8, 4.0, 100, s2);
        p3.setCategories(List.of(alcohol, beverages));
        productRepository.create(p1);
        productRepository.create(p2);
        productRepository.create(p3);
    }
}
