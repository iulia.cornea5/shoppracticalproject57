package org.example.repository;

import org.example.entity.Branch;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;


public class BranchRepository extends GenericAbstractRepository<Branch> {

    public BranchRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    // select t from Branch t
    public String getEntityName() {
        return "Branch";
    }

    public Class<Branch> getEntityClass() {
        return Branch.class;
    }

    public List<Branch> findByLocation(String location) {
        Session session = sessionFactory.openSession();
        String hql = "select b from Branch b where location like '" + location + "'";
        List<Branch> branches = session
                .createQuery(hql, Branch.class)
                .getResultList();
        session.close();
        return branches;
    }

    public List<Branch> findByWordInName(String word) {
        Session session = sessionFactory.openSession();
        String hql = "select b from Branch b where b.name like :wordParam";
        List<Branch> branches = session
                .createQuery(hql, Branch.class)
                .setParameter("wordParam", "%" + word + "%")
                .getResultList();
        session.close();
        return branches;
    }


}
