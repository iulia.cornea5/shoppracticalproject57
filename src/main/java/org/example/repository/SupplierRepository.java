package org.example.repository;

import org.example.entity.Supplier;
import org.hibernate.SessionFactory;

public class SupplierRepository extends GenericAbstractRepository<Supplier> {

    public SupplierRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Class getEntityClass() {
        return Supplier.class;
    }

    @Override
    public String getEntityName() {
        return "Supplier";
    }
}
