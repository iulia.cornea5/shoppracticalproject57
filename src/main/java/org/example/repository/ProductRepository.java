package org.example.repository;

import org.example.entity.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ProductRepository extends GenericAbstractRepository<Product> {

    public ProductRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getEntityName() {
        return "Product";
    }

    @Override
    public Class<Product> getEntityClass() {
        return Product.class;
    }


    public List<Product> readAllWithCategories() {
        Session session = sessionFactory.openSession();
        List<Product> products =
                session.createQuery("select p from Product p left join fetch p.categories",
                                Product.class)
                        .getResultList();
        session.close();
        return products;
    }

    public List<Product> getAllByPrice(Double maxPrice) {
        Session session = sessionFactory.openSession();
        List<Product> products =
                session.createQuery("select p from Product p left join fetch p.categories where p.price <= " + maxPrice, Product.class).getResultList();
        session.close();
        return products;
    }
}
