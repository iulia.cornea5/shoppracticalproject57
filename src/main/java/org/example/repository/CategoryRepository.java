package org.example.repository;

import org.example.entity.Category;
import org.hibernate.SessionFactory;

public class CategoryRepository extends GenericAbstractRepository<Category> {

    public CategoryRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getEntityName() {
        return "Category";
    }

    @Override
    public Class<Category> getEntityClass() {
        return Category.class;
    }
}
