package org.example.repository;

import org.example.entity.Employee;
import org.example.entity.enums.Job;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class EmployeeRepository extends GenericAbstractRepository<Employee> {


    public EmployeeRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public String getEntityName() {
        return "Employee";
    }

    @Override
    public Class<Employee> getEntityClass() {
        return Employee.class;
    }

    // cautam dupa un enum, nu dupa un string
    public List<Employee> findByJob(Job word) {
        Session session = sessionFactory.openSession();
        String hql = "select e from Employee e where e.job like :wordParam";
        List<Employee> employees = session.createQuery(hql, Employee.class)
                .setParameter("wordParam", word)
                .getResultList();
        session.close();
        return employees;
    }
}
