package org.example;

import org.example.config.HibernateConfiguration;
import org.example.entity.*;
import org.example.entity.enums.Job;
import org.example.repository.*;

import java.util.List;

public class MainManyToOne {

    public static void main(String[] args) {

        BranchRepository branchRepository = new BranchRepository(HibernateConfiguration.getSessionFactory());
        EmployeeRepository employeeRepository = new EmployeeRepository(HibernateConfiguration.getSessionFactory());

        Branch b1 = new Branch(null, "Metro-Cluj", "Cluj-Napoca, str Dorobanților 20");
        Branch b2 = new Branch(null, "Metro-Brașov", "Brașov, strada Primaverii 15");
        branchRepository.create(b1);
        branchRepository.create(b2);


        Employee employee1 = new Employee(null, "Buda Cristian", Job.CASHIER, b1);
        Employee employee2 = new Employee(null, "Marius Dumitrache", Job.MANAGER, b2);
        Employee employee3 = new Employee(null, "Marian Frizeru", Job.OTHER, b2);
        Employee employee4 = new Employee(null, "Ombladon Cheloo", Job.CASHIER, b2);
        employeeRepository.create(employee1);
        employeeRepository.create(employee2);
        employeeRepository.create(employee3);
        employeeRepository.create(employee4);

        // 1. afișați în consolă toți employees frumos (nume, job, branch)
        System.out.println(employeeRepository.readAll());

        // 2. similar cu ce am facut pentru employee - branch
        //    implementati pentru product - supplier    tot     ManyToOne

        SupplierRepository supplierRepository = new SupplierRepository(HibernateConfiguration.getSessionFactory());
        ProductRepository productRepository = new ProductRepository(HibernateConfiguration.getSessionFactory());
        CategoryRepository categoryRepository = new CategoryRepository(HibernateConfiguration.getSessionFactory());

        Supplier s1 = new Supplier(null, "Dorna", "Vatra Dornei");
        Supplier s2 = new Supplier(null, "Acvila", "Luduș");
        supplierRepository.create(s1);
        supplierRepository.create(s2);

        Product p1 = new Product(null, "Apa minerala", "Dorna", 2.0, 3.0, 500, s1);
        Product p2 = new Product(null, "Apa plata", "Dorna", 2.0, 3.0, 1000, s1);
        Product p3 = new Product(null, "Bere", "Tuborg", 2.0, 4.0, 100, s2);
        productRepository.create(p1);
        productRepository.create(p2);
        productRepository.create(p3);

        System.out.println(productRepository.readAll());

    }

}
