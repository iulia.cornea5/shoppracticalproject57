package org.example.config;

import org.example.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfiguration {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Supplier.class)
                    .addAnnotatedClass(Client.class)
                    .addAnnotatedClass(Branch.class)
                    .addAnnotatedClass(Employee.class)
                    .addAnnotatedClass(Category.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
