package org.example.ex.generics;

public abstract class GenericDataHandler<T> {

    public double calcularePretVandare(
            double pretAchizitie,
            double cantitate,
            double adaosComercial) {
        return pretAchizitie * cantitate * (1 + adaosComercial);
    }

    public abstract String comp(T o1, T o2);


}
